<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/16
 * Time: 11:13
 */

namespace app\admin\controller;


use think\Db;

class Config extends AdminController
{


    public  function index(){
        return $this->fetch("config/index");
    }


    public  function getGroup(){
        $list=Db::name("config_group")->order("order_sort desc ")->select();
        return json($list);
    }


    public  function  saveOrUpdateGroup(){
        if(request()->isPost()){
            $data['group_name']=request()->post("group_name");
            $data['order_sort']= request()->post("order_sort",0);
            $data['title']=request()->post("title");
            $rowcount=Db::name("config_group")->insertGetId($data);
            if($rowcount>0){
                return $this->check_success("操作成功！");
            }else{
                return $this->check_error("操作失败！");
            }
        }else{
          return  $this->fetch("config/saveOrUpdateGroup");
        }
    }


    public  function  validGroup(){
        $param=request()->post("param");
        $resultFlag=Db::name("config_group")->where(['group_name'=>$param])->count()>0?true:false;
        $status=$resultFlag?'n':'y';
        $info=$resultFlag?'组编码已经存在,请重新输入!':'组编码可以使用';
        return json( $returnJson=["info"=>$info,"status"=>$status,"validate"=>1]);
    }

    public  function  validValue(){
        $param=request()->post("param");
        $resultFlag=Db::name("config_value")->where(['name'=>$param])->count()>0?true:false;
        $status=$resultFlag?'n':'y';
        $info=$resultFlag?'变量名已经存在,请重新输入!':'变量名可以使用';
        return json( $returnJson=["info"=>$info,"status"=>$status,"validate"=>1]);
    }


    public  function  targetTabs(){
        $type=request()->post("type");
        $this->assign("type",$type);
        $list=array();
        if($type!="addcfg"){
            $list= Db::name("config_value")->where(array('group_name'=>$type))->select();
        }
        $this->assign("list",$list);
       return $this->fetch("config/targetTabs");
    }

    public  function saveOrUpdateGroupValue(){
        if(request()->isPost()){
            $data['group_name']=request()->post("group_name");
            $data['type']=request()->post("type");
            $data['name']=request()->post("name");
            $data['title']=request()->post("title");
            $data['value']=request()->post("value");
            $data['centent']=request()->post("content");
            $groupEntity=Db::name("config_group")->where(array("group_name"=>$data['group_name']))->find();
            $data['group_id']=$groupEntity['id'];
            $rowcount=Db::name("config_value")->insertGetId($data);
            if($rowcount>0){
                return $this->check_success("操作成功！");
            }else{
                return $this->check_error("操作失败！");
            }
        }
    }


    public  function  editValue(){
        if(request()->isPost()){
            $postData=request()->post();
           foreach($postData as $key=>$value){
               $valueEentity= Db::name("config_value")->where(array("name"=>$key))->find();
               if($valueEentity['type']=='checkbox'){
                   $tempValue=request()->post($key."/a");
                   Db::name("config_value")->where(array("name"=>$key))->update(array('value'=>implode(",",$tempValue)));
               }else{
                   Db::name("config_value")->where(array("name"=>$key))->update(array('value'=>$value));
               }
           }
        }
        return $this->check_success("操作成功！");
    }


    public  function groupIndex(){

        return $this->fetch("config/groupIndex");
    }


    public  function groupIndexList(){
        $postData['title']=request()->post("title");
        $dataField['name'] ="config_group";
        $dataField['order']="id desc";
        $where="";
        if(!empty($postData['title'])){
            $where.=" title like '%".$postData['title']."%'";
        }
        $dataField['where']=$where;
        $resultData=$this->findPageInfo($dataField);
        $resultData['html']=$this->fetch("config/groupIndexList",['list'=>$resultData['list']]);
        $resultData['validate']=1;
        $json= json($resultData);
        return $json;
    }


    public  function groupSaveOrUpdate(){
            $is_add= request()->param("is_add");
            if(request()->isPost()){
                $data['group_name']=request()->post("group_name");
                $data['order_sort']= request()->post("order_sort",0);
                $data['title']=request()->post("title");
                $data['id']=request()->post("id");
                if($is_add==0){
                    $rowcount=Db::name("config_group")->insertGetId($data);
                    if($rowcount>0){
                        return $this->check_success("操作成功！");
                    }else{
                        return $this->check_error("操作失败！");
                    }

                }else if($is_add==1){
                    //更新
                    $rowcount=Db::name("config_group")->where(array("id"=>$data['id']))->update($data);
                    if($rowcount!==false){
                        return $this->check_success("操作成功！");
                    }else{
                        return $this->check_error("操作失败！");
                    }
                }
            }else{
                $id= request()->param("id");
                $entity= Db::name("config_group")->where(array("id"=>$id))->find();
                return $this->fetch("config/groupSaveOrUpdate",['is_add'=>$is_add,"m"=>$entity]);
            }
    }


    public  function  deleteGroup($id){
        try{
            Db::name("config_value")->where(array('group_id'=>$id))->delete();
            Db::name("config_group")->where(array("id"=>$id))->delete();
            return $this->check_success("操作成功！");
        }catch (Exception $e){
            return $this->check_error("操作失败！");
        }
    }


    public  function  batDeleteGroup($ids){
        $ids=implode(",",$ids);
        try{
            Db::name("config_value")->where(array('group_id'=>array("in",$ids)))->delete();
            Db::name("config_group")->where(array("id"=>array("in",$ids)))->delete();
            return $this->check_success("操作成功！");
        }catch (Exception $e){
            return $this->check_error("操作失败！");
        }
    }


    public  function  valueIndex(){
        $group_id=request()->param("id");
        $this->assign("group_id",$group_id);
        return $this->fetch("config/valueIndex");
    }


    public  function  valueIndexList(){
        $postData['group_id']=request()->post("group_id");
        $postData['title']=request()->post("title");
        $dataField['name'] ="config_value";
        $dataField['order']="id desc";
        $where="group_id=".$postData['group_id'];
        if(!empty($postData['title'])){
            $where.=" title like '%".$postData['title']."%'";
        }
        $dataField['where']=$where;
        $resultData=$this->findPageInfo($dataField);
        $resultData['html']=$this->fetch("config/valueIndexList",['list'=>$resultData['list']]);
        $resultData['validate']=1;
        $json= json($resultData);
        return $json;
    }


    public  function valueSaveOrUpdate(){
        $is_add=request()->param("is_add");
        if(request()->isPost()){
            $data['group_name']=request()->post("group_name");
            $data['type']=request()->post("type");
            $data['name']=request()->post("name");
            $data['title']=request()->post("title");
            $data['value']=request()->post("value");
            $data['centent']=request()->post("content");
            $data['id']=request()->post("id");
            $data['group_id']=request()->post("group_id");
            $data['group_name']=request()->post("group_name");
            if($is_add==0){
                $rowcount=Db::name("config_value")->insertGetId($data);
                if($rowcount>0){
                    return $this->check_success("操作成功！");
                }else{
                    return $this->check_error("操作失败！");
                }
            }elseif($is_add==1){
                $rowcount=Db::name("config_value")->where(array("id"=>$data['id']))->update($data);
                if($rowcount!==false){
                    return $this->check_success("操作成功！");
                }else{
                    return $this->check_error("操作失败！");
                }
            }
        }else{
            $id=request()->param("id");
            $this->assign("is_add",request()->param("is_add"));
            $this->assign("group_id",request()->param("group_id"));
            $entity= Db::name("config_value")->where(array('id'=>$id))->find();
            $this->assign("m",$entity);
            $groupEntity=Db::name("config_group")->where(array("id"=>request()->param("group_id")))->find();
            $this->assign("gm",$groupEntity);
           return $this->fetch("config/valueSaveOrUpdate");
        }
    }


    public  function  deleteValue($id){
        try{
            Db::name("config_value")->where(array('id'=>$id))->delete();
            return $this->check_success("操作成功！");
        }catch (Exception $e){
            return $this->check_error("操作失败！");
        }
    }


    public  function  batDeleteValue($ids){
        $ids=implode(",",$ids);
        try{
            Db::name("config_value")->where(array('id'=>array("in",$ids)))->delete();
            return $this->check_success("操作成功！");
        }catch (Exception $e){
            return $this->check_error("操作失败！");
        }
    }

}