<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/13
 * Time: 12:26
 */

namespace app\admin\controller;


use think\Db;

class Index extends AdminController
{
    public function index(){
        $userentity= session(config("session_admin_auth"));
        $this->assign("menus",$userentity['menus']);
        $this->assign("username",$userentity['username']);
        $this->assign("logintime",$userentity['logintime']);
        return $this->fetch("index/index");
    }




    public  function  demo(){
        return $this->fetch("index/demo");
    }


    public  function  demoIndexList(){
        $list=array();
        $curr=request()->post("currentPage",1);
        for($i=1;$i<10;$i++){
            $list[]=["id"=>$i*$curr,"name"=>"测试".$i*$curr,"age"=>1+$i*$curr,"sex"=>'男'];
        }
        $html=$this->fetch("index/demoIndexList",array('list'=>$list));
        $resultData['curr']=$curr;
        $resultData['pages']=10;
        $resultData['total']=100;
        $resultData['pageSize']=10;
        $resultData['html']=$html;
        $resultData['validate']=1;
        $json= json($resultData);
        return $json;
    }


    public  function userinfo(){
        $entity=Db::name("admin_user")->where(array("id"=>UID))->find();
        $this->assign("m",$entity);
        return $this->fetch("index/userinfo");
    }

    public  function  updateUserInfo(){
        $data['username']=request()->post("username");
        $data['password']=request()->post("password");
        $data['status']=request()->post("status");
        $data['realname']=request()->post("realname");
        $data['mobile']=request()->post("mobile");
        $data['email']=request()->post("email");
        $data['remark']=request()->post("remark");
        if(empty($data['password'])){
            unset($data['password']);
        }else{
            $data['password']=md5($data['password']);
        }
        //更新
        $rowcount=Db::name("admin_user")->where(array("id"=>UID))->update($data);
        if($rowcount!==false){
            return $this->check_success("操作成功！");
        }else{
            return $this->check_error("操作失败！");
        }
    }



    //邮箱测试
    public  function  demoEmail(){

        // 如果群发邮件 则传入数组即可
//        $emails=array('b1@baijunyao.com','b2@baijunyao.com');
//        send_email($emails,'邮件标题','邮件内容');
        // 发送单条邮件
        var_dump(send_email('475025551@qq.com','我在测试邮箱发送程序','<h1>我在测试邮箱发送程序</h1><p>你在最什么哎</p>'));
    }


    public function  pdf(){
        pdf();
    }


}