<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/14
 * Time: 0:25
 */

namespace app\admin\controller;


use think\Db;

class AdminMenu extends AdminController
{

    public  function index(){
        return $this->fetch("adminMenu/index");
    }


    /**
     * 异步加载数据
     */
    public  function indexlist(){
        $postData['title']=request()->post("title");
        $postData['pid']=request()->param("pid",0);
        $entity=Db::name("admin_menu")->where(array("id"=>$postData['pid']))->find();

        $dataField['name']="admin_menu";
        $dataField['order']="order_sort desc";
        $where=" pid=".$postData['pid'];
        if(!empty($postData['title'])){
            $where.=" and menu_name like '%".$postData['title']."%' ";
        }
        $dataField['where']=$where;
        $result=$this->findPageInfo($dataField);
        $this->assign("pid",$postData['pid']);
        $this->assign("menu_name",$entity['menu_name']);
        $result['html']=$this->fetch("adminMenu/indexlist",['list'=>$result['list']]);
        $json= json($result);
        return $json;
    }


    /**
     * 保存或更新
     */
    public  function saveOrUpdate(){

        $is_add= request()->param("is_add");
        if(request()->isPost()){
            $data['id']=request()->post("id");
            $data['menu_name']=request()->post("menu_name");
            $data['menu_url']=request()->post("menu_url");
            $data['pid']=request()->post("pid");
            $data['status']=request()->post("status");
            $data['is_show']=request()->post("is_show");
            $data['is_hot']=request()->post("is_hot");
            $data['order_sort']=request()->post("order_sort");
            $data['is_new']=request()->post("is_new");
            $data['remark']=request()->post("remark");
            $data['icon']=request()->post("icon");
            if($is_add==0){
                $data['add_time']=time();
                $rowcount=Db::name("admin_menu")->insertGetId($data);
                if($rowcount>0){
                   return $this->check_success("操作成功！");

                }else{
                    return $this->check_error("操作失败！");

                }
            }else if($is_add==1){
                //更新
                $rowcount=Db::name("admin_menu")->where(array('id'=>$data['id']))->update($data);
                if($rowcount!==false){
                    return  $this->check_success("操作成功！");
                }else{
                    return  $this->check_error("操作失败！");
                }
            }

        }else{
            $id= request()->param("id");
            $entity= Db::name("admin_menu")->where(array("id"=>$id))->find();
            $menuList=$this->getTree();
            $pid=request()->param("pid");
            $this->assign("pid",$pid);
            $this->assign("menuList",$menuList);
            return $this->fetch("adminMenu/saveOrUpdate",['is_add'=>$is_add,"m"=>$entity]);
        }
    }



    public  function getTree(){
        $list=Db::name("admin_menu")->field("id,menu_name,pid")->order("order_sort desc")->select();
        $treeList=array();
        $this->tree($list,0,1,$treeList);
        return $treeList;
    }

    /**
     * 无限级分类
     * @access public
     * @param Array $data     //数据库里获取的结果集
     * @param Int $pid
     * @param Int $count       //第几级分类
     * @return Array $treeList
     */
    private static function tree(&$data,$pid = 0,$count = 1,&$treeList=array()) {
        foreach ($data as $key => $value){
            if($value['pid']==$pid){
                $value['count'] = $count;
                $treeList []=$value;
                self::tree($data,$value['id'],$count+1,$treeList);
            }
        }
    }



    public  function  selectedIcon(){
        return $this->fetch("adminMenu/selectedIcon");
    }

    public  function scannerPid(){
        $pid=request()->post("pid");
        $entity=Db::name("admin_menu")->where(array("id"=>$pid))->find();
        return json(array("id"=>$entity["pid"],"status"=>1,"msg"=>"操作成功","validate"=>1));
    }


    /**
     * 删除
     * @param $id
     */
    public  function delete($id){
        return  Db::name("admin_menu")->where(array('id'=>$id))->delete()?$this->check_success("操作成功！"):$this->check_error("操作失败！");
    }



    /**
     * 批量删除
     * @param $ids
     */
    public  function batDelete($ids){
        $ids=implode(",",$ids);
        $whre=array();
        $whre['id']=array('in',$ids);
        $list=Db::name("admin_menu")->where($whre)->select();
        foreach($list as $key=>$value){
            $this->delete($value['id']);
        }
        return  $this->check_success("操作成功！");
    }

}