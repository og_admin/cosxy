<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/14
 * Time: 17:05
 */

namespace app\admin\controller;


use think\Db;
use think\image\Exception;

class AdminUser extends  AdminController
{

    public  function index(){
        return $this->fetch("adminUser/index");
    }

    /**
     * 异步加载数据
     */
    public  function indexlist(){


        $postData['title']=request()->post("title");
        $dataField['name'] ="admin_user";
        $dataField['order']="id desc";
        $where="";
        if(!empty($postData['title'])){
            $where.=" username like '%".$postData['title']."%' or realname like '%".$postData['title']."%'";
        }
        $dataField['where']=$where;
        $resultData=$this->findPageInfo($dataField);
        $this->assign("is_super_admin",is_super_admin());
        $resultData['html']=$this->fetch("adminUser/indexlist",['list'=>$resultData['list']]);
        $resultData['validate']=1;

        $json= json($resultData);
        return $json;
    }



    /**
     * 保存或更新
     */
    public  function saveOrUpdate(){
        $is_add= request()->param("is_add");

        if(request()->isPost()){

            $data['id']=request()->post("id");
            $data['username']=request()->post("username");
            $data['password']=request()->post("password");
            $data['status']=request()->post("status");
            $data['realname']=request()->post("realname");
            $data['mobile']=request()->post("mobile");
            $data['email']=request()->post("email");
            $data['remark']=request()->post("remark");
            $data['roleId']=isset($_REQUEST['roleId'])?$_REQUEST['roleId']:null;
            if($is_add==0){
                $data['create_time']=time();
                $data['password']=md5($data['password']);
                $accessData=$data['roleId'];
                unset($data['roleId']);
                $rowcount=Db::name("admin_user")->insertGetId($data);
                if(!empty($accessData)&&count($accessData)>0){
                    $accessRoleData=array();
                    foreach($accessData as $key=>$val){
                        $entity['admin_user_id']=$rowcount;
                        $entity['admin_role_id']=$val;
                        $entity['create_time']=time();
                        $accessRoleData[]=$entity;
                    }
                    Db::name("admin_access")->insertAll($accessRoleData);
                }
                if($rowcount>0){
                    return $this->check_success("操作成功！");
                }else{
                    return $this->check_error("操作失败！");
                }

            }else if($is_add==1){
                if(empty($data['password'])){
                    unset($data['password']);
                }else{
                    $data['password']=md5($data['password']);
                }
                $accessData=$data['roleId'];
                unset($data['roleId']);

                //更新
                $rowcount=Db::name("admin_user")->where(array("id"=>$data['id']))->update($data);

                Db::name("admin_access")->where(array('admin_user_id'=>$data['id']))->delete();
                if(!empty($accessData)&&count($accessData)>0) {

                    $accessRoleData = array();
                    foreach ($accessData as $key => $val) {
                        $entity['admin_user_id'] = $data['id'];
                        $entity['admin_role_id'] = $val;
                        $entity['create_time'] = time();
                        $accessRoleData[] = $entity;
                    }

                    Db::name("admin_access")->insertAll($accessRoleData);
                }
                if($rowcount!==false){
                    return $this->check_success("操作成功！");
                }else{
                    return $this->check_error("操作失败！");
                }
            }


        }else{
            $id= request()->param("id");
            $roleList=Db::name("admin_role")->order("order_sort desc")->select();
            $accessList=Db::name("admin_access")->where(array('admin_user_id'=>$id))->select();

            $newlAccessList=array();
            foreach($accessList as $key=>$value){
                $newlAccessList[]=$value['admin_role_id'];
            }

            foreach($roleList as $key=>$value){
                if(in_array($value['id'],$newlAccessList)){
                    $roleList[$key]['is_check']=1;
                }else{
                    $roleList[$key]['is_check']=0;
                }
            }
            $entity= Db::name("admin_user")->where(array("id"=>$id))->find();
            $this->assign("roleList",$roleList);
            return $this->fetch("adminUser/saveOrUpdate",['is_add'=>$is_add,"m"=>$entity]);
        }
    }

    /**
     * 删除
     * @param $id
     */
    public  function delete($id){
        try{
            Db::name("admin_access")->where(array('admin_user_id'=>$id))->delete();
            Db::name("admin_user")->where(array("id"=>$id))->delete();
            return $this->check_success("操作成功！");
        }catch (Exception $e){
            return $this->check_error("操作失败！");
        }
    }


    /**
     * 批量删除
     * @param $ids
     */
    public  function batDelete($ids){
        $ids=implode(",",$ids);
        try{
            Db::name("admin_access")->where(array('admin_user_id'=>array("in",$ids)))->delete();
            Db::name("admin_user")->where(array("id"=>array("in",$ids)))->delete();
            return $this->check_success("操作成功！");
        }catch (Exception $e){
            return $this->check_error("操作失败！");
        }
    }

    public  function validUserName(){
        $param=request()->post("param");
        $is_add=request()->param("is_add");
        $id=request()->param("id");
        if($is_add==0){
            $resultFlag=Db::name("admin_user")->where(['username'=>$param])->count()>0?true:false;
            $status=$resultFlag?'n':'y';
            $info=$resultFlag?'用户名已经存在,请重新输入!':'此用户名可以使用';
            return json( $returnJson=["info"=>$info,"status"=>$status,"validate"=>1]);
        }else{
            $entity= Db::name("admin_user")->where(array("id"=>$id))->find();
            if($entity['username']==$param){
                return json( $returnJson=["info"=>"此用户名可以使用","status"=>"y","validate"=>1]);
            }else{
                $resultFlag=Db::name("admin_user")->where(['username'=>$param])->count()>0?true:false;
                $status=$resultFlag?'n':'y';
                $info=$resultFlag?'用户名已经存在,请重新输入!':'此用户名可以使用';
                return json( $returnJson=["info"=>$info,"status"=>$status,"validate"=>1]);
            }
        }
    }


    /**
     * 设置密码
     */
    public  function  setPassword(){
        $is_add=request()->param("is_add");
        if(request()->isPost()){
            $data['password']=request()->post("password");
            $data['password']=md5($data['password']);
            $data['id']=request()->post("id");
            $rowcount=Db::name("admin_user")->where(array('id'=>$data['id']))->update(["password"=>$data['password']])!==false?true:false;
            return $rowcount!==false?$this->check_success("操作成功！"):$this->check_error("操作失败！");
        }else{
            $id=request()->param("id");
            $userEntity=Db::name("admin_user")->where(array("id"=>$id))->find();
            $this->assign("m",$userEntity);
            $this->assign("is_add",$is_add);
            return $this->fetch("adminUser/setPassword");
        }
    }

}