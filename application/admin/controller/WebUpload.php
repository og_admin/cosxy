<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/28
 * Time: 19:26
 */

namespace app\admin\controller;


use think\Db;

class WebUpload extends  AdminController
{


    public  function  index(){
        $uid=UID;
        $this->assign("uid",$uid);
        $callback=request()->param("callback");
        $callback=empty($callback)?"callback":$callback;
        $callbackUploadsId=request()->param("callbackUploadsId");
        $callbackUploadsId=empty($callbackUploadsId)?"callbackUploadsId":$callbackUploadsId;
        $inputFileName=request()->param("inputFileName");
        $inputFileName=empty($inputFileName)?"inputFileName":$inputFileName;

        $this->assign("callbackUploadsId",$callbackUploadsId);
        $this->assign('callback',$callback);
        $this->assign('inputFileName',$inputFileName);
        return $this->fetch('webUpload/index');
    }

    public  function  upload(){
//关闭缓存
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        $name=$_POST['name'];   //文件名称
        $type=$_POST['type'];  //文件类型
        $lastModifiedDate=$_POST['lastModifiedDate'];//上传时间
        $size=$_POST['size'];    //文件大小
        $chunks=isset($_POST['chunks'])?$_POST['chunks']:1; //分片总数
        $_POST['chunks']=$chunks;
        $chunk=isset($_POST['chunk'])?$_POST['chunk']:0;  //当前分片数
        $_POST['chunk']=$chunk;
        $uploader = new WebUploadFile();

        if(($path = $uploader->upload('file', $_POST)) !== false){

            if($chunk==($chunks-1)){
                $ext=$this->get_ext($_POST['name']);
                $data=$uploader->chunksMerge($_POST['chunks'], $ext);
                if($data['status'] !=0){
                    $saveData['file_ext']=$ext;
                    $saveData['file_size']=$size;
                    $saveData['original_filename']=$name;
                    $saveData['current_filename']=$data['currentname'];
                    $saveData['savepath']=$data['url'];
                    $saveData['url']=$data['url'];
                    $saveData['create_time']=time();
                    $saveData['group_name']=$this->getGroupName($ext);
                    $saveData['sha1']=sha1_file(__ROOT__.$data['url']);
                    $saveData['md5']=md5_file(__ROOT__.$data['url']);
                    $uploadFileID= Db::name("uploadfile")->insertGetId($saveData);
                    $data['uploadFileId']=$uploadFileID;
                    $data['original_filename']=$_POST['name'];
                    $data['group_name']=$this->getGroupName($ext);
                    $data['data_check']=1;
                   return json(array('status'=>0,'msg'=>"上传成功",'data'=>$data));
                }
            }else{
                return json(array('status'=>2,'msg'=>"上传中". $name . " chunk:" . $chunk,'data'=>array()));
            }

        }else{
            return json(array('status'=>1,'msg'=>"上传格式错误",'data'=>array()));
        }





    }

    /**
     * 分组
     * @param $ext
     * @return string
     */
    private  function getGroupName($ext){
        if (in_array($ext, array('gif', 'jpg', 'jpeg', 'bmp', 'png', 'png'))) {
            return "images";
        }
        return "file";
    }
   private  function get_ext($file_name){
        return $fileEx=strtolower(substr(strrchr($file_name,"."),1));
    }

}