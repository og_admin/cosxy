<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/1/14
 * Time: 11:07
 */

namespace app\admin\controller;
use think\Controller;
use think\Db;

class AdminController extends  Controller
{


    public  function _initialize(){

        $this->checkAuth();
        $this->getSiteConfig();
        $userentity=session(config("session_admin_auth"));
        define('UID',$userentity["id"]);
    }

    function getSiteConfig(){
        $config_list=\think\Cache::get("config_list");
        if(empty($config_list)){
            $list=\think\Db::name("config_value")->select();
        }
        $newData=array();
        foreach($list as $key=>$value){
            $newData[$value['name']]=$value['value'];
        }

        $this->assign("site",$newData);
    }



    /**
     * 验证
     * @return bool
     */
    public  function checkAuth(){
        $is_super_admin=config("is_super_admin");
        $session_admin_auth=config("session_admin_auth");
        $userEntity=session($session_admin_auth);
        if(empty($userEntity)){
            $this->redirect(url("admin/publics/index"));
            exit;
        }
        //超级管理员无需验证
        if($userEntity['id']==$is_super_admin){
            return true;
        }

        //验证url地址
        $currentUrl=request()->controller()."/".request()->action();
        $currentUrl=strtolower($currentUrl);
        $checkUrlFlag=true;
        if (!in_array($currentUrl,$userEntity['auth'])){
            $checkUrlFlag=false;
        }

        //验证通过
        if($checkUrlFlag){
            return true;
        }

        if(request()->isAjax()){
            echo json_encode(array('validate'=>0,"msg"=>"无权限","status"=>0));
            exit;
        }

        if(request()->isGet()){
            $this->error("无权限",url("Publics/index"));
            exit;
        }





    }



    public function check_success($msg, $data = array())
    {
        return json(array("validate" => 1, "msg" => $msg, "status" => 1, "data" => $data));
    }

    public function check_error($msg, $data = array())
    {
        return json(array("validate" => 1, "msg" => $msg, "status" => 0, $data));
    }

    /**
     * 查询分页方法
     * @param $dataField['name'] 表名称 必须传递
     * @param $dataField['pageSize'] 每页显示数量 默认25
     * @param $dataField['currentPage'] 当前页数 默认1
     * @param  $dataField['where'] 条件 可以不传递
     * @param $dataField['join'] 连接查询
     * @param $dataField['order'] 排序
     * @param $dataField['field'] 字段
     */
    public  function findPageInfo($dataField){
        $name=$dataField['name'];
        $pageSize=isset($dataField['pageSize'])?$dataField['pageSize']:10;
        $curr=request()->post("currentPage",1);
        $where=isset($dataField['where'])?$dataField['where']:array();
        $join=isset($dataField['join'])?$dataField['join']:array();
        $order=isset($dataField['order'])?$dataField['order']:array();
        $field=isset($dataField['field'])?$dataField['field']:" * ";
        $alias=isset($dataField['alias'])?$dataField['alias']:"";
        $group=isset($dataField['group'])?$dataField['group']:null;
        $resultTotal=Db::name($name)->alias($alias)->field(" count(*) count_total ")->where($where)->join($join)->page($curr)->group($group)->order($order)->find();
        $count_total=$resultTotal['count_total'];
        $totalPage=ceil($count_total/$pageSize);
        if($curr>$totalPage){
            $curr=$totalPage;
        }
        $list=Db::name($name)->alias($alias)->field($field)->where($where)->join($join)->limit($pageSize)->page($curr)->group($group)->order($order)->select();
//           echo Db::name($name)->getLastSql();

        $resultData['curr']=$curr;
        $resultData['pages']=$totalPage;
        $resultData['total']=$count_total;
        $resultData['pageSize']=$pageSize;
        $resultData['html']="";
        $resultData["list"]=$list;
        return $resultData;

    }


}
?>