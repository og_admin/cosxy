<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/17
 * Time: 11:05
 */

namespace app\admin\controller;


class QRcode extends AdminController
{


    public  function create($url,$size=4){
        vendor("phpqrcode/phpqrcode");
        $url=urldecode($url);
        \QRcode::png($url,false,QR_ECLEVEL_L,$size,2,false,0xFFFFFF,0x000000);
    }
}