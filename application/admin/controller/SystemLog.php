<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/15
 * Time: 1:06
 */

namespace app\admin\controller;


class SystemLog extends  AdminController
{

    public  function index(){
        return $this->fetch("systemLog/index");
    }

    /**
     * 异步加载数据
     */
    public  function indexlist(){
        $postData['title']=request()->post("title");
        $dataField['name'] ="system_log";
        $dataField['order']="id desc";
        $where="";
        if(!empty($postData['title'])){
            $where.=" module_name like '%".$postData['title']."%' or username like '%".$postData['title']."%'";
        }
        $dataField['where']=$where;
        $resultData=$this->findPageInfo($dataField);
        $resultData['html']=$this->fetch("systemLog/indexlist",['list'=>$resultData['list']]);
        $resultData['validate']=1;
        $json= json($resultData);
        return $json;
    }

}