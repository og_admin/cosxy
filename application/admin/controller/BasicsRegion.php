<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/1/26
 * Time: 18:18
 */

namespace app\admin\controller;

use app\admin\service\BasicsRegionService;
use think\Cache;
use think\Db;
use think\image\Exception;
use think\Request;

/**
 * 区域管理
 * Class BasicsRegion
 * @package app\admin\controller
 */
class BasicsRegion extends  AdminController
{

    /**
     * 配置管理首页
     */
    public  function index(){
        return $this->fetch("basicsRegion/index");
    }

    /**
     * 异步加载数据
     */
    public  function indexlist(){
        $postData['title']=request()->post("title");
        $postData['parent_id']=request()->post("parent_id",0);
        $dataField['name'] ="basics_region";
        $dataField['order']="id desc";
        $where=" parent_id=".$postData['parent_id'];
        if(!empty($postData['title'])){
            $where=" region_name like '%".$postData['title']."%'";
        }

        $dataField['where']=$where;
        $result=$this->findPageInfo($dataField);
        $enptu=Db::name("basics_region")->where(array('id'=>$postData['parent_id']))->find();
        $this->assign("parent_id",$postData['parent_id']);
        $this->assign("region_name",$enptu['region_name']);
        $result['html']=$this->fetch("basicsRegion/indexlist",['list'=>$result['list']]);
        $json= json($result);
        return $json;
    }

    /**
     * 保存或更新
     */
    public  function saveOrUpdate(){
        $is_add= request()->param("is_add");

        if( request()->isPost()){
            $data['id']= request()->request("id");
            $data['parent_id']= request()->request("parent_id");
            $data['region_name']= request()->request("region_name");
            $data['type_id']= request()->request("parent_id");
            $data['parent_id']= request()->request("parent_id");
            $data['center_postion']= request()->request("center_postion");
            $data['region_code']= request()->request("region_code");
            $rowcount=0;
            //0添加
            if($is_add==0){
                $rowcount=Db::name("basics_region")->insertGetId($data);
                if($rowcount>0){
                    return $this->check_success("操作成功！");
                }else{
                    return $this->check_success("操作失败！");
                }
            }else if($is_add==1){
                //更新
                $rowcount=Db::name("basics_region")->where(array("id"=>$data['id']))->update($data);
                if($rowcount>0){
                    return $this->check_success("操作成功！");
                }else{
                    return $this->check_success("操作失败！");
                }
            }

        }else{
            $list=Db::name("basics_region")->select();
            $treeList=array();
            $this->tree($list,0,1,$treeList);
            $this->assign("treelist",$treeList);
            $id=  request()->param("id");
            $parent_id=request()->param("parent_id");
            $this->assign("parent_id",$parent_id);
            $entity= Db::name("basics_region")->where(array("id"=>$id))->find();
            return $this->fetch("basicsRegion/saveOrUpdate",['is_add'=>$is_add,"m"=>$entity]);
        }
    }


    /**
     * 无限级分类
     * @access public
     * @param Array $data     //数据库里获取的结果集
     * @param Int $pid
     * @param Int $count       //第几级分类
     * @return Array $treeList
     */
    private static function tree(&$data,$pid = 0,$count = 1,&$treeList=array()) {
        foreach ($data as $key => $value){
            if($value['parent_id']==$pid){
                $value['count'] = $count;
                $treeList []=$value;
                self::tree($data,$value['id'],$count+1,$treeList);
            }
        }
    }


    /**
     * 删除
     * @param $id
     */
    public  function delete($id){
        try{
            $list= Db::name("basics_region")->where(array("parent_id"=>$id))->select();
            foreach($list as $key=>$value){
                Db::name("basics_region")->where(array("id"=>$value['id']))->delete();
            }
            Db::name("basics_region")->where(array("id"=>$id))->delete();
            return $this->check_success("操作成功！");
        }catch (Exception $E){
            return $this->check_error("操作失败！");
        }
    }

    /**
     * 批量删除
     * @param $ids
     */
    public  function batDelete($ids){

        try{
            $ids=implode(",",$ids);
            $where=array();
            $where["id"]=['in',$ids];
            $list=DB::name("basics_region")->where($where)->select();
            foreach($list as $key=>$value){
                $this->delete($value['id']);
            }
            return $this->check_success("操作成功！");
        }catch (Exception $e){
            return $this->check_error("操作失败！");
        }
    }

    /**
     * 获取上一级菜单
     */
    public  function scannerPid(){
        $parent_id=request()->post("parent_id");
        $entity=Db::name("basics_region")->where(array("id"=>$parent_id))->find();
        return json(array("id"=>$entity["parent_id"],"status"=>1,"msg"=>"操作成功","validate"=>1));
    }


    /**
     * 获取区域
     * @return \think\response\Json
     */
    public  function getBasicsRegion(){
        $id=input("request.id");
         return json(Db::name("basics_region")->where(array('parent_id'=>$id))->select());
    }


}