<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/15
 * Time: 14:40
 */

namespace app\admin\controller;


use think\Db;

class UploadFile extends  AdminController
{

    public  function index(){
        $callback=request()->param("callback");
        $callback=empty($callback)?"callback":$callback;
        $callbackUploadsId=request()->param("callbackUploadsId");
        $callbackUploadsId=empty($callbackUploadsId)?"callbackUploadsId":$callbackUploadsId;
        $inputFileName=request()->param("inputFileName");
        $inputFileName=empty($inputFileName)?"inputFileName":$inputFileName;

        $this->assign("callbackUploadsId",$callbackUploadsId);
        $this->assign('callback',$callback);
        $this->assign('inputFileName',$inputFileName);
        return $this->fetch("uploadFile/index");
    }


    /**
     * 上传文件
     * @param $serverFileEntity
     */
    public  function upload(){
        $rootPath= $_SERVER['DOCUMENT_ROOT'].config("uploads");
        //判断文件夹是否存在
        if(!is_dir($rootPath)){
            mkdir($rootPath,0777);
            chmod($rootPath,0777);
        }
        $saveFilePath=date("Ymd",time());
        if(!is_dir($rootPath.$saveFilePath)){
            mkdir($rootPath.$saveFilePath,0777);
            chmod($rootPath.$saveFilePath,0777);
        }
        $saveName=date("YmdHis",time()).$this->randrom();
        $file=request()->file("uploadFile");
        $file->setSaveName($saveFilePath.DS.$saveName);
        $info = $file->validate(array('ext'=>'jpg,gif,png,jpeg,zip,rar,tar,gz,7z,doc,docx,txt,xml,mp3,mp4'))->move($rootPath.$saveFilePath,$saveName);
        if($info){
            $ext= $info->getExtension();
            $saveData['file_ext']=$info->getExtension();
            $saveData['file_size']=$info->getSize();
            $saveData['original_filename']=$_FILES['uploadFile']['name'];
            $saveData['current_filename']=$saveName;
            $saveData['group_name']=$this->getGroupName($ext);
            $saveData['savepath']=config("uploads").$saveFilePath."/".$saveName.".".$ext;
            $saveData['url']=config("uploads").$saveFilePath."/".$saveName.".".$ext;
            $saveData['create_time']=time();
            $saveData['group_name']=$this->getGroupName($ext);
            $saveData['sha1']=sha1_file($rootPath.$saveFilePath.DS.$saveName.".".$ext);
            $saveData['md5']=md5_file($rootPath.$saveFilePath.DS.$saveName.".".$ext);
            $uploadFileID= Db::name("uploadfile")->insertGetId($saveData);
            $saveData['upload_rootpath']=config("uploads");
            $saveData['server_display_url']="";
            $saveData['uploadFileId']=$uploadFileID;
            //是否选择
            $saveData['data_check']=1;

            if($uploadFileID!==false&&$uploadFileID>0){
                return array('status'=>1,"msg"=>"上传成功","data"=>$saveData,'time'=>time());
            }else{
                return array('status'=>0,"msg"=>"上传记录保存失败！","data"=>$saveData,'time'=>time());
            }
        }else{
            // 上传失败获取错误信息
            return array('status'=>0,"msg"=>$file->getError(),'time'=>time());
        }

    }


    /**
     * 分组
     * @param $ext
     * @return string
     */
    private  function getGroupName($ext){
        if (in_array($ext, array('gif', 'jpg', 'jpeg', 'bmp', 'png', 'png'))) {
            return "images";
        }
        return "file";
    }


    /**
     * 随机生成
     * @return string
     */
    private  function randrom(){
        $arr=array();
        while(count($arr)<5)
        {
            $arr[]=rand(1,10);
            $arr=array_unique($arr);
        }
        return implode("",$arr);
    }



    /**
     * 预览上传图片
     */
    public  function previewUploads(){
        $list= Db::name("uploadfile")->group("file_ext")->select();
        $this->assign('list',$list);
        $callback=request()->param("callback");
        $callback=empty($callback)?"callback":$callback;
        $callbackUploadsId=request()->param("callbackUploadsId");
        $callbackUploadsId=empty($callbackUploadsId)?"callbackUploadsId":$callbackUploadsId;
        $inputFileName=request()->param("inputFileName");
        $inputFileName=empty($inputFileName)?"inputFileName":$inputFileName;

        $this->assign("callbackUploadsId",$callbackUploadsId);
        $this->assign('callback',$callback);
        $this->assign('inputFileName',$inputFileName);
        return $this->fetch("uploadFile/previewUploads");
    }


    /**
     * 上传
     * @return \think\response\Json
     */
    public  function  previewUploadsList(){

        $postData['fileName']=request()->post("fileName");
        $postData['startDate']=request()->post("startDate");
        $postData['endDate']=request()->post("endDate");
        $postData['fileType']=request()->post("fileType");

        $dataField['name'] ="uploadfile";
        $dataField['pageSize']=12;
        $dataField['order']="create_time desc,id desc";
        $where=" 1=1 ";
        if(!empty($postData['fileName'])){
            $where.=" and current_filename like '%".$postData['fileName']."%' or original_filename like '%".$postData['fileName']."%'";
        }

        if(!empty($postData['startDate'])&&!empty($postData['endDate'])){
            $where.=" and create_time between ".strtotime($postData['startDate'])." and ".strtotime($postData['endDate']);
        }else{
            if(!empty($postData['startDate'])){
                $where.=" and create_time>=".strtotime($postData['startDate']);
            }

            if(!empty($postData['endDate'])){
                $where.=" and create_time<=".strtotime($postData['endDate']);
            }
        }
        if(!empty($postData['fileType'])){
            $where.=" and file_ext='".$postData['fileType']."' ";
        }

        $dataField['where']=$where;
        $resultData=$this->findPageInfo($dataField);
        $resultData['validate']=1;
        $resultData['html']=$this->fetch("uploadFile/previewUploadsList",['list'=>$resultData['list']]);
        $json= json($resultData);
        return $json;
    }


    public  function getUploadsOne(){
        $id=request()->post("id");
        $uploadEntity= Db::name("uploadfile")->where(['id'=>$id])->find();

        $uploadEntity['upload_rootpath']=config("uploads");
        $uploadEntity['server_display_url']="";
        $uploadEntity['uploadFileId']=$uploadEntity["id"];
        //是否选择
        $uploadEntity['data_check']=1;
        return json($uploadEntity);

    }



    public  function uploadIndex(){
        return $this->fetch("uploadFile/uploadIndex");
    }

    public  function uploadIndexList(){
        $postData['title']=request()->post("title");
        $dataField['name'] ="uploadfile";
        $dataField['order']="id desc";
        $where="";
        if(!empty($postData['title'])){
            $where.=" original_filename like '%".$postData['title'];
        }
        $dataField['where']=$where;
        $resultData=$this->findPageInfo($dataField);
        $resultData['html']=$this->fetch("uploadFile/uploadIndexList",['list'=>$resultData['list']]);
        $resultData['validate']=1;

        $json= json($resultData);
        return $json;
    }




}