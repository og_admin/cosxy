<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/17
 * Time: 11:05
 */

namespace app\admin\controller;

use Endroid\QrCode\QrCode;
use think\Response;

class QRcodeUtil extends AdminController
{


    public  function create($url,$size=4){
        vendor("phpqrcode/phpqrcode");
        $url=urldecode($url);
        \QRcode::png($url,false,QR_ECLEVEL_L,$size,2,false,0xFFFFFF,0x000000);
    }


    /**
     * 二维码生成
     */
    public function qrcode()
    {
            vendor("endroid/qrcode/src/QrCode");
            $text = $this->request->get('text',"http://www.baidu.com");
            $size = $this->request->get('size');
            $padding = $this->request->get('padding');
            $errorcorrection = $this->request->get('errorcorrection',"quartile");
            $foreground = $this->request->get('foreground', "#ffffff");
             $background = $this->request->get('background', "#000000");
            $logo = $this->request->get('logo',0);
            $logosize = $this->request->get('logosize',300);
            $label = $this->request->get('label',"");
            $labelfontsize = $this->request->get('labelfontsize',14);
            $labelhalign = $this->request->get('labelhalign',0);
            $labelvalign = $this->request->get('labelvalign',3);


            // 前景色
            list($r, $g, $b) = sscanf($foreground, "#%02x%02x%02x");
            $foregroundcolor = ['r' => $r, 'g' => $g, 'b' => $b];

            // 背景色
            list($r, $g, $b) = sscanf($background, "#%02x%02x%02x");
            $backgroundcolor = ['r' => $r, 'g' => $g, 'b' => $b];

            $qrCode = new  QrCode();
            $qrCode
                ->setText($text)
                ->setSize($size)
                ->setPadding($padding)
                ->setErrorCorrection($errorcorrection)
                ->setForegroundColor($foregroundcolor)
                ->setBackgroundColor($backgroundcolor)
                ->setLogoSize($logosize)
                ->setLabelFontPath(ROOT_PATH . 'public/assets/fonts/fzltxh.ttf')
                ->setLabel($label)
                ->setLabelFontSize($labelfontsize)
                ->setLabelHalign($labelhalign)
                ->setLabelValign($labelvalign)
                ->setImageType(QrCode::IMAGE_TYPE_PNG);
            if ($logo)
            {
                $qrCode->setLogo(ROOT_PATH . 'public/assets/img/qrcode.png');
            }
            //也可以直接使用render方法输出结果
            //$qrCode->render();
            return new Response($qrCode->get(), 200, ['Content-Type' => $qrCode->getContentType()]);

    }
}