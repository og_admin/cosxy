<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/14
 * Time: 16:09
 */

namespace app\admin\controller;


use think\Db;
use think\image\Exception;

class AdminRole extends  AdminController
{


    /**
     * 配置管理首页
     */
    public  function index(){

        return $this->fetch("adminRole/index");
    }

    /**
     * 异步加载数据
     */
    public  function indexlist(){
        $postData['title']=request()->post("title");
        $dataField['name'] ="admin_role";
        $dataField['order']="a1.order_sort desc";
        $where="1=1 ";
        if(!empty($postData['title'])){
            $where.=" and a1.name like '%".$postData['title']."%'";
        }
        $dataField['alias']="a1";
        $dataField['field']="a1.*";
        $dataField['where']=$where;
        $resultData=$this->findPageInfo($dataField);
        $resultData['html']=$this->fetch("adminRole/indexlist",['list'=>$resultData['list']]);
        $json= json($resultData);
        return $json;
    }

    /**
     * 保存或更新
     */
    public  function saveOrUpdate(){
        $is_add= request()->param("is_add");

        if(request()->isPost()){
            $data['id']=request()->post("id");
            $data['name']=request()->post("name");
            $data['order_sort']=request()->post("order_sort");
            $data['status']=request()->post("status");
            $data['remark']=request()->post("remark");
            if($is_add==0){
                $data['create_time']=time();
                $rowcount=Db::name("admin_role")->insertGetId($data);
                if($rowcount>0){
                   return $this->check_success("操作成功！");
                }else{
                    return $this->check_error("操作失败！");
                }
            }else if($is_add==1){
                //更新
                $rowcount=Db::name("admin_role")->where(array('id'=>$data['id']))->update($data);
                if($rowcount!==false){
                    return $this->check_success("操作成功！");
                }else{
                    return $this->check_error("操作失败！");
                }
            }
        }else{
            $id= request()->param("id");
            $entity= Db::name("admin_role")->where(array("id"=>$id))->find();
            return $this->fetch("adminRole/saveOrUpdate",['is_add'=>$is_add,"m"=>$entity]);
        }
    }

    /**
     * 删除
     * @param $id
     */
    public  function delete($id){
        try{
            Db::name("admin_access")->where(array('admin_role_id'=>$id))->delete();
            Db::name("admin_role")->where(array("id"=>$id))->delete();
            return $this->check_success("操作成功！");
        }catch (Exception $e){
            return $this->check_error("操作失败！");
        }
    }

    /**
     * 批量删除
     * @param $ids
     */
    public  function batDelete($ids){
        $ids=implode(",",$ids);
        try{
            Db::name("admin_access")->where(array('admin_role_id'=>array("in",$ids)))->delete();
            DB::name("admin_role")->where(array('id'=>array('in',$ids)))->delete();
            return $this->check_success("操作成功！");
        }catch (Exception $e){
            return $this->check_error("操作失败！");
        }
    }

    /**
     * 授权
     */
    public  function haveMenusRole(){
        $id=request()->param("id");
        $entity=Db::name("admin_role")->where(array("id"=>$id))->find();
        $this->assign("m",$entity);
        return $this->fetch("adminRole/haveMenusRole");
    }


    /**
     * 获取拥有权限
     */
    public  function authGroupRulesAjax(){
        $id=request()->param("id");
        $list=Db::name("admin_menu")->field("id,menu_name as name,pid as pId,menu_url")->where(array('status'=>1))->order("order_sort desc")->select();
        $roleEntity=Db::name("admin_role")->where(array("id"=>$id))->find();
        $have_menus=$roleEntity['have_menus'];
        $have_menusArray=explode(",",$have_menus);
        foreach($list as $key=>$value){
            $list[$key]["checked"]=in_array($value["id"],$have_menusArray);
        }
        return json($list);
    }

    /**
     * 写权限操作
     */
    public  function wirteAuthGroup(){
        $rules=request()->post("rules");
        $id=request()->post("id");
        return Db::name("admin_role")->where(array("id"=>$id))->update(array('have_menus'=>$rules))!==false?$this->check_success("操作成功！"):$this->check_error("操作失败！");

    }
}