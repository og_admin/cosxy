<?php

function  print_nbsp($count = 0)
{
    $nbsp = "";
    for ($i = 0; $i < $count; $i++) {
        $nbsp .= "&nbsp;&nbsp;&nbsp;";
    }
    return $nbsp;
}

//是不是超级管理员
function is_super_admin(){
    return config("is_super_admin");
}

/**
 * 日志
 * @param $remark
 */
function logSave($remark)
{
    $session_admin_auth = config("session_admin_auth");
    $userEntity = session($session_admin_auth);
    $module_name = request()->module();
    $data['module_name'] = $module_name;
    $data['create_time'] = time();
    $data['request_ip'] = request()->ip();
    $data['admin_user_id'] = $userEntity['id'];
    $data['username'] = $userEntity['username'];
    $data['remark'] = "在" . date("Y年m月d日H是i分s", time()) . "【" . $userEntity['username'] . "】用户，执行了【" . request()->module() . "/" . request()->controller() . "/" . request()->action() . "】方法，传递的参数：" . $remark;
    \think\Db::name("system_log")->insertGetId($data);
}



function printrCheck($name,$str,$checkValue){
    $array=explode(",",$str);
    $returnStr="";

    foreach($array as $key=>$value){
        $temp=explode("|",$value);
        $selecStr="";
        if(in_array($temp[1],explode(",",$checkValue))){
            $selecStr="checked";
        }
        $returnStr.='<div class="checkbox-inline i-checks"><label><input type="checkbox"  '.$selecStr.' value="'.$temp[1].'" name="'.$name.'[]"> <i></i> '.$temp[0].'</label></div>';
    }
    return $returnStr;
}

function printrRadio($name,$str,$checkValue){
    $array=explode(",",$str);
    $returnStr="";
    foreach($array as $key=>$value){
        $temp=explode("|",$value);
        $selecStr="";
        if($temp[1]==$checkValue){
            $selecStr="checked";
        }
        $returnStr.='<div class="radio-inline i-checks"><label><input type="radio"  '.$selecStr.' value="'.$temp[1].'" name="'.$name.'"> <i></i> '.$temp[0].'</label></div>';
    }
    return $returnStr;
}

function printrSelect($name,$str,$checkValue){
    $array=explode(",",$str);
    $returnStr="<select name='".$name."'>";
    foreach($array as $key=>$value){
        $temp=explode("|",$value);
        $selecStr="";
        if($temp[1]==$checkValue){
            $selecStr="selected";
        }
        $returnStr.='<option value="'.$temp[1].'" '.$selecStr.'>'.$temp[0].'</option>';
    }
    $returnStr=$returnStr."</select>";
    return $returnStr;
}

