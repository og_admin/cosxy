/**
 * Created by Administrator on 2017/7/14.
 */
$(function(){
        $("#submitLogin").click(function(){
              var username=$("#pd-form-username");
              var password=$("#pd-form-password");
              var code=$("#pd-form-code");
              if(username.val()==""){
                  ydlMsg("请输入用户名！");
                  username.focus();
                  return ;
              }
              if(password.val()==""){
                  ydlMsg("请输入密码！");
                  password.focus();
                  return ;
              }
              if(code.val()==""){
                  ydlMsg("请输入验证码！");
                  code.focus();
                  return ;
              }

            var action=$("#login-form").attr("action");
            ydlAjax(action,$("#login-form").serializeArray(),function(resultObj){
                if(resultObj.status==1){
                    ydlMsg(resultObj.msg);
                    window.location.href=resultObj.url;
                }else{
                    ydlMsg(resultObj.msg);
                    setTimeout(function(){
                        window.location.reload();
                    },1000);
                }
            });
        });
        $("#code-ca").click(function(){
            var str=$(this).attr("datat-dj");
            var  str=str.substring(0,str.lastIndexOf("."));
            $(this).attr("src",str+"/s/"+new Date().getTime());
        });
});