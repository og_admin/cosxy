/**
 * Created by Administrator on 2017/7/13.
 */
var Backend = {
    api: {
        sidebar: function (params) {
            colorArr = ['red', 'green', 'yellow', 'blue', 'teal', 'orange', 'purple'];
            $colorNums = colorArr.length;
            badgeList = {};
            $.each(params, function (k, v) {
                $url = Backend.api.fixurl(k);

                if ($.isArray(v))
                {
                    $nums = typeof v[0] !== 'undefined' ? v[0] : 0;
                    $color = typeof v[1] !== 'undefined' ? v[1] : colorArr[(!isNaN($nums) ? $nums : $nums.length) % $colorNums];
                    $class = typeof v[2] !== 'undefined' ? v[2] : 'label';
                } else
                {
                    $nums = v;
                    $color = colorArr[(!isNaN($nums) ? $nums : $nums.length) % $colorNums];
                    $class = 'label';
                }
                //必须nums大于0才显示
                badgeList[$url] = $nums > 0 ? '<small class="' + $class + ' pull-right bg-' + $color + '">' + $nums + '</small>' : '';
            });
            $.each(badgeList, function (k, v) {
                var anchor = top.window.$("li a[addtabs][url='" + k + "']");
                if (anchor) {
                    top.window.$(".pull-right-container", anchor).html(v);
                    top.window.$(".nav-addtabs li a[node-id='" + anchor.attr("addtabs") + "'] .pull-right-container").html(v);
                }
            });
        },
        addtabs: function (url, title, icon) {
            console.info(url);
            //var dom = "a[url='{url}']"
            //var leftlink = top.window.$(dom.replace(/\{url\}/, url));
            //if (leftlink.size() > 0) {
            //    leftlink.trigger("click");
            //} else {
            //    url = Backend.api.fixurl(url);
            //    leftlink = top.window.$(dom.replace(/\{url\}/, url));
            //    if (leftlink.size() > 0) {
            //        var event = leftlink.parent().hasClass("active") ? "dblclick" : "click";
            //        leftlink.trigger(event);
            //    } else {
            //        var baseurl = url.substr(0, url.indexOf("?") > -1 ? url.indexOf("?") : url.length);
            //        leftlink = top.window.$(dom.replace(/\{url\}/, baseurl));
            //        //能找到相对地址
            //        if (leftlink.size() > 0) {
            //            icon = typeof icon != 'undefined' ? icon : leftlink.find("i").attr("class");
            //            title = typeof title != 'undefined' ? title : leftlink.find("span:first").text();
            //            leftlink.trigger("fa.event.toggleitem");
            //        }
            //        var navnode = $(".nav-tabs ul li a[node-url='" + url + "']");
            //        if (navnode.size() > 0) {
            //            navnode.trigger("click");
            //        } else {
            //            //追加新的tab
            //            var id = Math.floor(new Date().valueOf() * Math.random());
            //            icon = typeof icon != 'undefined' ? icon : 'fa fa-circle-o';
            //            title = typeof title != 'undefined' ? title : '';
            //            top.window.$("<a />").append('<i class="' + icon + '"></i> <span>' + title + '</span>').prop("href", url).attr({url: url, addtabs: id}).addClass("hide").appendTo(top.window.document.body).trigger("click");
            //        }
            //    }
            //}
        }


    },
    init: function () {
        //点击加入到Shortcut
        $(document).on('click', '#ribbon ol li:last a[data-url]', function (e) {
            e.preventDefault();
            var fastjump = top.window.$(".fastmenujump");
            if (fastjump) {
                var url = $(this).data("url");
                var text = $(this).text();
                if (fastjump.find("option[value='" + url + "']").size() == 0) {
                    fastjump.append("<option value='" + url + "'>" + $(this).text() + "</option>");
                    var shortcut = localStorage.getItem("shortcut");
                    shortcut = shortcut ? JSON.parse(shortcut) : {};
                    shortcut[url] = text;
                    localStorage.setItem("shortcut", JSON.stringify(shortcut));
                    toastr.success(__('Operation completed'));
                }
            } else {
                toastr.error(__('Operation failed'));
            }
        });
        //修复含有fixed-footer类的body边距
        if ($(".fixed-footer").size() > 0) {
            $(document.body).css("padding-bottom", $(".fixed-footer").height());
        }
    }
};


Backend.init();