/**
 * Created by Administrator on 2017/7/14.
 */
$(function(){
        $("#submitLogin").click(function(){
              var username=$("#pd-form-username");
              var password=$("#pd-form-password");
              var yanzhengma=$("#yanzhengma");
              if(username.val()==""){
                  ydlMsg("请输入用户名！");
                  username.focus();
                  return ;
              }
              if(password.val()==""){
                  ydlMsg("请输入密码！");
                  password.focus();
                  return ;
              }
              if(yanzhengma.val()==""){
                  ydlMsg("请输入验证码！");
                  yanzhengma.focus();
                  return ;
              }

            var action=$("#login-form").attr("action");
            ydlAjax(action,$("#login-form").serializeArray(),function(resultObj){
                if(resultObj.code==1){
                    ydlMsg(resultObj.msg);
                    window.location.href=resultObj.url;
                }else{
                    ydlMsg(resultObj.msg);
                    setTimeout(function(){
                        window.location.reload();
                    },1000);
                }
            });
        });

       $("#code-ca").click(function(){
           var str=$(this).attr("datat-dj");
           var  str=str.substring(0,str.lastIndexOf("."));
           $(this).attr("src",str+"/s/"+new Date().getTime());
       });
});


function  ydlAjax(url,paramdata,callBack){
    $.ajax({
        type: "post",
        url: url,
        data:paramdata,
        dataType: "json",
        error:function(request) {//请求出错
            ydl_error("网络异常，请求失败");
        },
        success: function(resultObject){
            callBack(resultObject);
        },
    });
}